package com.erikzuo.sentiaandroidtest.adpater

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.erikzuo.sentiaandroidtest.R
import com.erikzuo.sentiaandroidtest.mvp.model.Property
import com.erikzuo.sentiaandroidtest.ui.PropertyDetailActivity
import com.erikzuo.sentiaandroidtest.ui.PropertyDetailFragment
import com.erikzuo.sentiaandroidtest.ui.PropertyListActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.property_item_premium.view.*

/**
 * Created by YifanZuo on 14/1/18.
 */
class PropertyListAdapter(private val mParentActivity: PropertyListActivity,
                          private val mValues: List<Property>,
                          private val mTwoPane: Boolean) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_PREMIUM = 0
        const val TYPE_STANDARD = 1
    }

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val property = v.tag as Property
            if (mTwoPane) {
                val fragment = PropertyDetailFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(PropertyDetailFragment.PROPERTY_ITEM, property)
                    }
                }

                mParentActivity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.property_detail_container, fragment)
                        .commit()
            } else {
                val intent = Intent(v.context, PropertyDetailActivity::class.java).apply {
                    putExtra(PropertyDetailFragment.PROPERTY_ITEM, property)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder =
                when (viewType) {
                    TYPE_PREMIUM ->
                        PremiumViewHolder(
                                LayoutInflater.from(parent.context).inflate((R.layout.property_item_premium), parent, false))

                    else ->
                        StandardViewHolder(
                                LayoutInflater.from(parent.context).inflate((R.layout.property_item), parent, false))
                }


        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val property = mValues[position]

        when (getItemViewType(position)) {
            TYPE_PREMIUM ->
                (holder as PremiumViewHolder).bindViews(property)

            else ->
                (holder as StandardViewHolder).bindViews(property)

        }


        with(holder.itemView) {
            tag = property
            setOnClickListener(mOnClickListener)
        }


    }


    override fun getItemCount(): Int {
        return mValues.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (mValues[position].is_premium) TYPE_PREMIUM else TYPE_STANDARD
    }


    interface ViewHolder {
        fun bindViews(property: Property)
    }

    inner class PremiumViewHolder(mView: View) : RecyclerView.ViewHolder(mView), ViewHolder {
        val mTitle: TextView = mView.title
        val mAddress: TextView = mView.address
        val mBedrooms: TextView = mView.bedrooms
        val mBathrooms: TextView = mView.bathrooms
        val mCarspots: TextView = mView.carspots
        val mOwnerName: TextView = mView.owner_name
        val mOwnerAvatar: ImageView = mView.owner_avatar
        val mPhoto: ImageView = mView.photo

        override fun bindViews(property: Property) {
            mOwnerName.text = property.owner.first_name + " " + property.owner.last_name
            mBedrooms.text = property.bedrooms.toString()
            mBathrooms.text = property.bathrooms.toString()
            mCarspots.text = property.carspots.toString()
            mTitle.text = property.title
            mAddress.text = property.location.full_address

            Picasso.with(mParentActivity)
                    .load(property.owner.avatar.url)
                    .fit()
                    .centerCrop()
                    .into(mOwnerAvatar)


            Picasso.with(mParentActivity)
                    .load(property.photo.image.url)
                    .fit()
                    .centerCrop()
                    .into(mPhoto)


        }
    }

    inner class StandardViewHolder(mView: View) : RecyclerView.ViewHolder(mView), ViewHolder {
        val mTitle: TextView = mView.title
        val mAddress: TextView = mView.address
        val mOwnerName: TextView = mView.owner_name
        val mOwnerAvatar: ImageView = mView.owner_avatar

        override fun bindViews(property: Property) {
            mOwnerName.text = property.owner.first_name + " " + property.owner.last_name
            mTitle.text = property.title
            mAddress.text = property.location.full_address

            Picasso.with(mParentActivity)
                    .load(property.owner.avatar.url)
                    .fit()
                    .centerCrop()
                    .into(mOwnerAvatar)

        }
    }
}
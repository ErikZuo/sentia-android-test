package com.erikzuo.sentiaandroidtest.ui

 import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
 import com.erikzuo.sentiaandroidtest.mvp.model.MainModel
import com.erikzuo.sentiaandroidtest.mvp.presenter.BasePresenter
import rx.Observable
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription


/**
 * Created by YifanZuo on 14/1/18.
 */

open abstract class BaseActivity<Presenter: BasePresenter<*>> : AppCompatActivity() {
    private var mCompositeSubscription: CompositeSubscription? = null
    protected var mPresenter: Presenter? = null


    protected abstract fun createPresenter(context: Context): Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPresenter = createPresenter(this)
        mPresenter!!.onCreate()
    }

    override fun onDestroy() {
        onUnsubscribe()
        mPresenter!!.onDestroy()

        super.onDestroy()
    }


    fun addSubscription(observable: Observable<MainModel>, subscriber: Subscriber<MainModel>) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = CompositeSubscription()
        }
        mCompositeSubscription!!.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber))
    }

    fun addSubscription(subscription: Subscription) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = CompositeSubscription()
        }
        mCompositeSubscription!!.add(subscription)
    }

    fun onUnsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription!!.hasSubscriptions())
            mCompositeSubscription!!.unsubscribe()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {

            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }

    }

}

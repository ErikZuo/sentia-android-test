package com.erikzuo.sentiaandroidtest.ui

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import com.erikzuo.sentiaandroidtest.R
import com.erikzuo.sentiaandroidtest.adpater.PropertyListAdapter
import com.erikzuo.sentiaandroidtest.mvp.model.MainModel
import com.erikzuo.sentiaandroidtest.mvp.model.Property
import com.erikzuo.sentiaandroidtest.mvp.presenter.PropertyListPresenter
import com.erikzuo.sentiaandroidtest.mvp.view.MainView
import kotlinx.android.synthetic.main.progress_bar.*
import kotlinx.android.synthetic.main.property_list.*
import java.util.ArrayList


class PropertyListActivity : BaseActivity<PropertyListPresenter>(), MainView {


    private var mTwoPane: Boolean = false
    private var mPropertyList = ArrayList<Property>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_list)

        if (property_detail_container != null) {
            mTwoPane = true
        }

        if (savedInstanceState == null) {
            mPresenter!!.loadData()
        }else{
            mPropertyList = savedInstanceState?.getParcelableArrayList(PROPERTY_LIST)
            setupRecyclerView(mPropertyList)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)


        outState?.putParcelableArrayList(PROPERTY_LIST, mPropertyList)
    }


    override fun createPresenter(context: Context): PropertyListPresenter {
        return PropertyListPresenter(this)
    }


    private fun setupRecyclerView(list: List<Property>) {
        property_list.adapter = PropertyListAdapter(this, list, mTwoPane)
    }

    override fun showLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress_bar.visibility = View.GONE

    }

    override fun getDataSuccess(model: MainModel) {
        Log.d("success", model.data.size.toString())

        mPropertyList = model.data as ArrayList<Property>
        setupRecyclerView(mPropertyList)

    }

    override fun getDataFail(msg: String) {
        Log.d("success", msg)

    }

    companion object {
        const val PROPERTY_LIST = "property_list"
    }

}



package com.erikzuo.sentiaandroidtest.ui

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.erikzuo.sentiaandroidtest.R
import com.erikzuo.sentiaandroidtest.mvp.model.Property
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_property_detail.*

class PropertyDetailActivity : AppCompatActivity() {
    private var mItem: Property? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_detail)
        setSupportActionBar(detail_toolbar)

        mItem = intent.getParcelableExtra(PropertyDetailFragment.PROPERTY_ITEM)

        mItem?.let {
            supportActionBar?.title = it.title
            Picasso.with(this).load(it.photo.image.url).into(photo_image)
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity using a fragment transaction.
            val fragment = PropertyDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(PropertyDetailFragment.PROPERTY_ITEM,
                            intent.getParcelableExtra(PropertyDetailFragment.PROPERTY_ITEM))
                }
            }

            supportFragmentManager.beginTransaction()
                    .add(R.id.property_detail_container, fragment)
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    NavUtils.navigateUpTo(this, Intent(this, PropertyListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }


}

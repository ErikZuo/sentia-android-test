package com.erikzuo.sentiaandroidtest.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.erikzuo.sentiaandroidtest.R
import com.erikzuo.sentiaandroidtest.mvp.model.Property
import kotlinx.android.synthetic.main.activity_property_detail.*
import kotlinx.android.synthetic.main.property_detail.view.*

class PropertyDetailFragment : Fragment() {

    private var mItem: Property? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null){
            mItem = savedInstanceState.getParcelable(PROPERTY_ITEM)

            mItem?.let {
                activity?.toolbar_layout?.title = it.title
            }
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.property_detail, container, false)

         mItem?.let {
            rootView.property_detail.text = it.description
        }

        return rootView
    }

    companion object {
        const val PROPERTY_ITEM = "property_item"
    }
}

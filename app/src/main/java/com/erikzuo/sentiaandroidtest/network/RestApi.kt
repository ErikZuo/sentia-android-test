package com.erikzuo.sentiaandroidtest.network

import com.erikzuo.sentiaandroidtest.mvp.model.MainModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable

/**
 * Created by YifanZuo on 14/1/18.
 */
object RestApi {
    private val propertyApi: PropertyApi

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(PropertyApi.API_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()

        propertyApi = retrofit.create(PropertyApi::class.java)
    }


    fun loadPropertyList(): Observable<MainModel> {
        return propertyApi.loadProperties()
    }
}
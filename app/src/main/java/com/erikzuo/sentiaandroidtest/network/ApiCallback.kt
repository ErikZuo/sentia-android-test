package com.erikzuo.sentiaandroidtest.network

import retrofit2.HttpException
import rx.Subscriber
 import android.util.Log


/**
 * Created by YifanZuo on 14/1/18.
 */
abstract class ApiCallback<M> : Subscriber<M>() {

    abstract fun onSuccess(model: M)

    abstract fun onFailure(msg: String)

    abstract fun onFinish()


    override fun onError(e: Throwable) {
        e.printStackTrace()
        if (e is HttpException) {

            val code = e.code()
            var msg = e.message
            Log.d("code", "code=" + code)

            onFailure(msg + code)
        } else {
            onFailure(e.message!!)
        }
        onFinish()
    }

    override fun onNext(model: M) {
        onSuccess(model)

    }

    override fun onCompleted() {
        onFinish()
    }
}
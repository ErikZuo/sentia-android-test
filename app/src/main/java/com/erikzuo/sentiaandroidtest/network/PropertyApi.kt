package com.erikzuo.sentiaandroidtest.network

import com.erikzuo.sentiaandroidtest.mvp.model.MainModel
import retrofit2.http.GET
import rx.Observable

/**
 * Created by YifanZuo on 14/1/18.
 */
interface PropertyApi {

    companion object {
        //baseUrl
        val API_SERVER_URL = "http://demo0065087.mockable.io/"
    }


    @GET("test/properties")
    fun loadProperties() : Observable<MainModel>

}
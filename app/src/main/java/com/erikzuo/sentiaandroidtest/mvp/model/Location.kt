package com.erikzuo.sentiaandroidtest.mvp.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by YifanZuo on 14/1/18.
 */
class Location(val id : Int,
               val address_1 :String,
               val address_2 :String,
               val suburb : String,
               val state :String,
               val postcode :String,
               val country :String,
               val latitude :Double,
               val longitude :Double,
               val full_address : String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(address_1)
        parcel.writeString(address_2)
        parcel.writeString(suburb)
        parcel.writeString(state)
        parcel.writeString(postcode)
        parcel.writeString(country)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(full_address)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }
}
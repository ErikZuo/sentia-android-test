package com.erikzuo.sentiaandroidtest.mvp.presenter

import android.support.annotation.CallSuper
import com.erikzuo.sentiaandroidtest.mvp.model.MainModel
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription


/**
 * Created by YifanZuo on 14/1/18.
 */
open abstract class BasePresenter<V> {
    var mView: V? = null
    private var mCompositeSubscription: CompositeSubscription? = null


    @CallSuper
    fun onCreate() {
    }


    @CallSuper
    fun onResume() {
    }


    @CallSuper
    fun onPause() {
    }


    @CallSuper
    fun onDestroy() {
    }


    fun attachView(mvpView: V) {
        this.mView = mvpView
    }


    fun detachView() {
        this.mView = null
        onUnsubscribe()
    }


    fun onUnsubscribe() {
        if (mCompositeSubscription != null && mCompositeSubscription!!.hasSubscriptions()) {
            mCompositeSubscription!!.unsubscribe()
        }
    }


    fun addSubscription(observable: Observable<MainModel>, subscriber: Subscriber<MainModel>) {
        if (mCompositeSubscription == null) {
            mCompositeSubscription = CompositeSubscription()
        }

        mCompositeSubscription!!.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber))
    }
}
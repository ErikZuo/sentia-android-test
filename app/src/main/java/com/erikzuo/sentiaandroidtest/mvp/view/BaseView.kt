package com.erikzuo.sentiaandroidtest.mvp.view

/**
 * Created by YifanZuo on 14/1/18.
 */
interface BaseView{
    fun showLoading()

    fun hideLoading()
}
package com.erikzuo.sentiaandroidtest.mvp.model

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

/**
 * Created by YifanZuo on 14/1/18.
 */
open class Property(val id: Int,
                    val title: String,
                    val description: String,
                    val is_premium: Boolean,
                    val state: String,
                    val bedrooms: Int,
                    val bathrooms: Int,
                    val carspots: Int,
                    val price: Double,
                    val owner: Owner,
                    val location: Location,
                    val photo: Photo) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readParcelable(Owner::class.java.classLoader),
            parcel.readParcelable(Location::class.java.classLoader),
            parcel.readParcelable(Photo::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeByte(if (is_premium) 1 else 0)
        parcel.writeString(state)
        parcel.writeInt(bedrooms)
        parcel.writeInt(bathrooms)
        parcel.writeInt(carspots)
        parcel.writeDouble(price)
        parcel.writeParcelable(owner, flags)
        parcel.writeParcelable(location, flags)
        parcel.writeParcelable(photo, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Property> {
        override fun createFromParcel(parcel: Parcel): Property {
            return Property(parcel)
        }

        override fun newArray(size: Int): Array<Property?> {
            return arrayOfNulls(size)
        }
    }

}

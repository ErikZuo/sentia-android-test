package com.erikzuo.sentiaandroidtest.mvp.presenter

import com.erikzuo.sentiaandroidtest.mvp.model.MainModel
import com.erikzuo.sentiaandroidtest.mvp.view.MainView
import com.erikzuo.sentiaandroidtest.network.ApiCallback
import com.erikzuo.sentiaandroidtest.network.RestApi


/**
 * Created by YifanZuo on 14/1/18.
 */
class PropertyListPresenter(view: MainView) : BasePresenter<MainView>() {

    init {
        attachView(view)
    }




    fun loadData() {
        mView!!.showLoading()
        addSubscription(RestApi.loadPropertyList(),
                object : ApiCallback<MainModel>() {
                    override fun onSuccess(model: MainModel) {
                        mView!!.getDataSuccess(model)
                    }

                    override fun onFailure(msg: String) {
                        mView!!.getDataFail(msg)
                    }


                    override fun onFinish() {
                        mView!!.hideLoading()
                    }

                })
    }

}

package com.erikzuo.sentiaandroidtest.mvp.view

import com.erikzuo.sentiaandroidtest.mvp.model.MainModel

/**
 * Created by YifanZuo on 14/1/18.
 */
interface MainView : BaseView {
    fun getDataSuccess(model: MainModel)

    fun getDataFail(msg: String)
}